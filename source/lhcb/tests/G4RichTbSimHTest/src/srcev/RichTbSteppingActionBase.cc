// Include files

// local

//-----------------------------------------------------------------------------
// Implementation file for class : RichTbSteppingActionBase
//
// 2015-03-06 : Sajan Easo
//-----------------------------------------------------------------------------
#include "RichTbSteppingActionBase.hh"
#include "G4SteppingManager.hh"
#include "globals.hh"

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RichTbSteppingActionBase::RichTbSteppingActionBase() { InitRichTbStepActions(); }
//=============================================================================
// Destructor
//=============================================================================
RichTbSteppingActionBase::~RichTbSteppingActionBase() {

  if (mRichTbSteppingAction)
    delete mRichTbSteppingAction;
  if (mRichTbPmtSteppingAction)
    delete mRichTbPmtSteppingAction;
}

//=============================================================================
void RichTbSteppingActionBase::UserSteppingAction(const G4Step *aStep) {

  mRichTbSteppingAction->UserSteppingAction(aStep);
  mRichTbPmtSteppingAction->UserSteppingAction(aStep);
}

void RichTbSteppingActionBase::InitRichTbStepActions() {

  mRichTbSteppingAction = new RichTbSteppingAction();
  mRichTbPmtSteppingAction = new RichTbPmtSteppingAction();
}
