//  Rich Test Beam Simulation   Main program
// ----------------------------------------------------------------
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4VPhysicalVolume.hh"
#include "RichTbEventAction.hh"
#include "RichTbIOData.hh"
#include "RichTbPhysicsList.hh"
#include "RichTbPrimaryGeneratorAction.hh"
#include "RichTbRunAction.hh"
#include "RichTbRunConfig.hh"
#include "RichTbStackingAction.hh"
#include "RichTbSteppingActionBase.hh"
#include "RichTbTrackingAction.hh"
#include "RichTbUpgradeDetectorConstruction.hh"
#include "RichTbVisManager.hh"

#ifdef G4UI_USE_XM
#include "G4UIXm.hh"
#endif
#include "Randomize.hh"
#include "RichTbAnalysisManager.hh"
#include "RichTbVisManager.hh"

#include "G4ios.hh"
#include <cstdlib>
#include <iostream>

int main(int argc, char **argv) {

  // Seed the random number generator manually
  // ------------------------------------------
  // G4long myseed = 345354;
  G4long myseed = 755365;
  //  HepRandom::setTheSeed(myseed);

  // Run manager
  // G4RunManager *runManager = new G4RunManager;
  // Job and Run  options.

  RichTbRunConfig *rConfig = RichTbRunConfig::getRunConfigInstance();
  myseed = rConfig->getRandomSeedInput();
  G4cout << "Current random number seed " << myseed << G4endl;

  CLHEP::HepRandom::setTheSeed(myseed);

  // Run manager
  G4RunManager *runManager = new G4RunManager;

  // Datafile streams for input and output
  RichTbIOData *rIOData = RichTbIOData::getRichTbIODataInstance();

  // Setup the analysis stuff.

  RichTbAnalysisManager *analysisManager = RichTbAnalysisManager::getInstance();

  if (rIOData)
    G4cout << " IO Data initialized " << G4endl;
  if (analysisManager)
    G4cout << " Analysis manager initialized " << G4endl;

  //#ifdef G4VIS_USE
  // visualization manager
  RichTbVisManager *visManager = RichTbVisManager::getRichTbVisManagerInstance();
  //  visManager->SetVerboseLevel(0);

  visManager->Initialise();
  // #endif

  // UserInitialization classes - mandatory
  RichTbDetectorConstruction *RichTbDet = new RichTbDetectorConstruction();
  runManager->SetUserInitialization(RichTbDet);
  RichTbPhysicsList *RichTbPhy = new RichTbPhysicsList();
  runManager->SetUserInitialization(RichTbPhy);

  // UserAction classes - optional

  runManager->SetUserAction(new RichTbRunAction());
  RichTbPrimaryGeneratorAction *PrimaryGenAction = new RichTbPrimaryGeneratorAction();
  runManager->SetUserAction(PrimaryGenAction);
  RichTbEventAction *eventAction = new RichTbEventAction();
  runManager->SetUserAction(eventAction);
  runManager->SetUserAction(new RichTbStackingAction);
  RichTbSteppingActionBase *StepActionBase = new RichTbSteppingActionBase();
  runManager->SetUserAction(StepActionBase);

  runManager->SetUserAction(new RichTbTrackingAction);

  G4UImanager *UI = G4UImanager::GetUIpointer();

  G4UIsession *session = 0;
  //   UI->ApplyCommand("/run/verbose 2");
  // UI->ApplyCommand("/control/verbose 2");

  // Initialize G4 kernel
  runManager->Initialize();
  // Test for running in batch mode
  bool batchmode = false;
  G4int numEvInBatch = 1;

  if (rConfig->getBatchModeFlag() > 0) {
    batchmode = true;
    numEvInBatch = rConfig->getNumEventInBatchMode();
  }

  if (batchmode) {
    runManager->BeamOn(numEvInBatch);
    // runManager->BeamOn(200);
  } else {
    // ALL GUI stuff commented off for now.
    // get the pointer to the User Interface manager
    //  G4UImanager* UI = G4UImanager::GetUIpointer();
    // G4UIsession* session=0;
    // User interactions
    // Define (G)UI for interactive mode
    if (argc == 1) {
      //   #ifdef G4UI_USE_XM
      // session = new G4UIXm(argc,argv);
      //#else
      // G4UIterminal is a (dumb) terminal.
      session = new G4UIterminal;
      //#endif
    }
    //#ifdef G4VIS_USE
    // visualization manager
    //  G4VisManager* visManager = new RichTbVisManager();
    // RichTbVisManager* visManager = new RichTbVisManager();
    // visManager->SetVerboseLevel(0);
    // visManager->Initialize();
    // G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
    //  std::cout << " PVVisManager " << pVVisManager <<  std::endl;

    ///#endif

    if (session) { // Interactive mode
#ifdef G4UI_USE_XM
      // Customize the G4UIXm menubar with a macro file :
      UI->ApplyCommand("/control/execute /afs/cern.ch/user/s/seaso/mycmt/RichTb/v1/macro/gui.mac");
#endif
      UI->ApplyCommand("/run/verbose 0");
      UI->ApplyCommand("/event/verbose 0");
      UI->ApplyCommand("/tracking/verbose 2");
      UI->ApplyCommand("/particle/process/verbose 0");

      // runManager->BeamOn(1);

      session->SessionStart();
      delete session;
    } else { // Batch mode
      G4String command = "/control/execute ";
      G4String fileName = argv[1];
      G4UImanager::GetUIpointer()->ApplyCommand(command + fileName);
    }
  }

  //#ifdef G4VIS_USE
  delete visManager;
  // #endif

  G4cout << "\nVisManager deleted..\n" << G4endl;

  //  delete analysisManager;

  delete runManager;

  G4cout << "\nRunManager deleted..\n" << G4endl;

  return 0;
}
