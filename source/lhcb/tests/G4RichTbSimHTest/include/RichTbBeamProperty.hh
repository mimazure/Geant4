// $Id: $
#ifndef INCLUDE_RICHTBBEAMPROPERTY_HH
#define INCLUDE_RICHTBBEAMPROPERTY_HH 1

// Include files
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4ParticleDefinition.hh"

/** @class RichTbBeamProperty RichTbBeamProperty.hh include/RichTbBeamProperty.hh
 *
 *
 *  @author Sajan EASO
 *  @date   2004-01-22
 */
class RichTbBeamProperty {
public:
  virtual ~RichTbBeamProperty(); ///< Destructor

  static RichTbBeamProperty *getRichTbBeamPropertyInstance();

  G4ThreeVector getBeamPosition() { return mBeamPosition; }
  G4ThreeVector getBeamDirection() { return mBeamDirection; }
  void setBeamPosition(G4ThreeVector aBeamPosition) { mBeamPosition = aBeamPosition; }
  void setBeamDirection(G4ThreeVector aBeamDirection) { mBeamDirection = aBeamDirection; }

  G4ThreeVector getNominalBeamPosition() { return mNominalBeamPosition; }

  void setNominalBeamPosition(G4ThreeVector aNominalBeamPosition) { mNominalBeamPosition = aNominalBeamPosition; }

  G4ThreeVector getNominalBeamDirectionCos() { return mNominalBeamDirectionCos; }
  void setNominalBeamDirectionCos(G4ThreeVector aDirCos) { mNominalBeamDirectionCos = aDirCos; }

  void setBeamPartDef(G4ParticleDefinition *aBeamPartDef) { mBeamPartDef = aBeamPartDef; }

  G4ParticleDefinition *getBeamPartDef() { return mBeamPartDef; }

  G4String BeamPartName() { return mBeamPartName; }
  void setBeamPartName(G4String aBname) { mBeamPartName = aBname; }

  G4ThreeVector getBeamPosUpstrAgel() { return mBeamPosUpstrAgel; }

  void setBeamPosUpstrAgel(G4ThreeVector aBPosA) { mBeamPosUpstrAgel = aBPosA; }

  G4ThreeVector getBeamDirUpstrAgel() { return mBeamDirUpstrAgel; }
  void setBeamDirUpstrAgel(G4ThreeVector aBDirA) { mBeamDirUpstrAgel = aBDirA; }

  G4ThreeVector getAgelNormal() { return mAgelNormal; }
  void setAgelNormal(G4ThreeVector aAgelNormal) { mAgelNormal = aAgelNormal; }

  G4ThreeVector getAgelBeamPostionLocal() { return mAgelBeamPostionLocal; }

  void setAgelBeamPostionLocal(G4ThreeVector aPosLocal) { mAgelBeamPostionLocal = aPosLocal; }

  void ResetBeamProperty();
  void PrintBeamProperty();

protected:
private:
  /// Standard constructor
  RichTbBeamProperty();

  static RichTbBeamProperty *RichTbBeamPropertyInstance;

  G4ThreeVector mBeamPosition;
  G4ThreeVector mBeamDirection;
  G4ThreeVector mNominalBeamPosition;
  G4ThreeVector mNominalBeamDirectionCos;
  G4ParticleDefinition *mBeamPartDef;
  G4String mBeamPartName;
  G4ThreeVector mBeamPosUpstrAgel;
  G4ThreeVector mBeamDirUpstrAgel;
  G4ThreeVector mAgelNormal;
  G4ThreeVector mAgelBeamPostionLocal;
};
#endif // INCLUDE_RICHTBBEAMPROPERTY_HH
