#ifndef RichTbStackingAction_h
#define RichTbStackingAction_h 1

#include "G4UserStackingAction.hh"
class RichTbStackingAction : public G4UserStackingAction {

public:
  RichTbStackingAction();
  virtual ~RichTbStackingAction();
  void NewStage() override { ; }
  void PrepareNewEvent() override { ; }

private:
};
#endif
