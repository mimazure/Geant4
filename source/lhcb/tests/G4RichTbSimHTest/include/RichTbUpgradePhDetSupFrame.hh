#ifndef INCLUDE_RICHTBUPGRADEPHDETSUPFRAME_HH
#define INCLUDE_RICHTBUPGRADEPHDETSUPFRAME_HH 1

// Include files
#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "RichTbUpgradeCrystalMaster.hh"

/** @class RichTbUpgradePhDetSupFrame RichTbUpgradePhDetSupFrame.hh include/RichTbUpgradePhDetSupFrame.hh
 *
 *
 *  @author Sajan Easo
 *  @date   2014-10-23
 */
class RichTbUpgradePhDetSupFrame {
public:
  /// Standard constructor
  RichTbUpgradePhDetSupFrame(RichTbUpgradeCrystalMaster *rTbCrystalMaster);

  virtual ~RichTbUpgradePhDetSupFrame(); ///< Destructor

  void constructRichTbPhotoDetectorSupFrame();
  void constructRichTbPhotoDetectorSupFrameWithHpd();
  void constructRichTbPhotoDetectorSupFrame15();

  G4LogicalVolume *getRichTbPhDetSupFrameLeftLVol() { return RichTbPhDetSupFrameLeftLVol; }
  G4LogicalVolume *getRichTbPhDetSupFrameRightLVol() { return RichTbPhDetSupFrameRightLVol; }
  G4VPhysicalVolume *getRichTbPhDetSupFrameLeftPVol() { return RichTbPhDetSupFrameLeftPVol; }
  G4VPhysicalVolume *getRichTbPhDetSupFrameRightPVol() { return RichTbPhDetSupFrameRightPVol; }

  G4LogicalVolume *getRichTbPhDetSupFrameBottomLeftLVol() { return RichTbPhDetSupFrameBottomLeftLVol; }
  G4LogicalVolume *getRichTbPhDetSupFrameBottomRightLVol() { return RichTbPhDetSupFrameBottomRightLVol; }
  G4VPhysicalVolume *getRichTbPhDetSupFrameBottomLeftPVol() { return RichTbPhDetSupFrameBottomLeftPVol; }
  G4VPhysicalVolume *getRichTbPhDetSupFrameBottomRightPVol() { return RichTbPhDetSupFrameBottomRightPVol; }

  RichTbUpgradeCrystalMaster *getCrystalMaster() { return aRTbCrystalMaster; }

protected:
private:
  G4LogicalVolume *RichTbPhDetSupFrameLeftLVol;
  G4LogicalVolume *RichTbPhDetSupFrameRightLVol;
  G4VPhysicalVolume *RichTbPhDetSupFrameLeftPVol;
  G4VPhysicalVolume *RichTbPhDetSupFrameRightPVol;
  G4LogicalVolume *RichTbPhDetSupFrameBottomLeftLVol;
  G4LogicalVolume *RichTbPhDetSupFrameBottomRightLVol;
  G4VPhysicalVolume *RichTbPhDetSupFrameBottomLeftPVol;
  G4VPhysicalVolume *RichTbPhDetSupFrameBottomRightPVol;
  RichTbUpgradeCrystalMaster *aRTbCrystalMaster;
};
#endif // INCLUDE_RICHTBUPGRADEPHDETSUPFRAME_HH
