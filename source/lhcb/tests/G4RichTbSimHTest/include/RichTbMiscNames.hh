// $Id: $
#ifndef INCLUDE_RICHTBMISCNAMES_HH
#define INCLUDE_RICHTBMISCNAMES_HH 1

const G4String PMTAnodeMaterialName = "PMTAnode";

const G4String PmtQuartzWMaterialName = "PmtWindowQuartz";
const G4String CrystalMaterialName = "CrystalMat";
const G4String PmtPhCathodeMaterialName = "S20PhCathode";
const G4String NitrogenGasMaterialName = "NitrogenGas";
const G4String c4f10GasMaterialName = "c4f10Gas";
const G4String PMTSMasterMaterialName = "Galactic";
const G4String HPDSMasterMaterialName = "Galactic";
const G4String ROgeometryName = "RichTbROGeom";
const G4String ROgeometryNameHpd = "RichTbROGeomHpd";
const G4String PMTSDname = "RichTbPMTSD";
const G4String HPDSDname = "RichTbHPDSD";
const G4String RichTbHColname = "RichTbHitsCollection";
const G4String RichTbHColnameHpd = "RichTbHpdHitsCollection";
const G4String VesselPhysName = "VesselPhys";
const G4String LensPhysName = "LensPhys";
const G4String MasterPhysName = "MasterPhys";
const G4String PMTEnvelopeBoxPhysName = "PMTEnvelopeBoxPhys";
const G4String PMTQuartzWPhysName = "PMTQuartzWPhys";
const G4String PMTPhCathodePhysName = "PMTPhCathodePhys";
const G4String PMTAnodePhysName = "PMTAnodePhys";
const G4String CrystalPhysName = "CrystalPhys";
const G4String CrystalMasterPhysName = "CrystalMasterPhys";
const G4String CrystalCoverPhysName = "CrystalCoverPhys";
const G4String DarkCoverPhysName = "DarkCoverPhys";
const G4String PhDFramePhysName = "PhDFramePhys";
const G4String MirrorPhysName = "MirrorPhys";
const G4String PhDetSupName = "PhDetSupPhys";

const G4String PhDetSupPhysNameLeft = "PhDetSupFrameLeftPhys";
const G4String PhDetSupPhysNameRight = "PhDetSupFrameRightPhys";
const G4String PhDetSupPhysNameBottomLeft = "PhDetSupFrameBottomLeftPhys";   // 2015 upgrade
const G4String PhDetSupPhysNameBottomRight = "PhDetSupFrameBottomRightPhys"; // 2015 upgrade

const G4String PMTSMasterPhysName = "PMTSMasterPhys";
const G4String PMTSiDetPhysName = "PMTSiDetPhys";
const G4String PMTQuartzPhysName = "PMTQuartzPhys";
const G4String PmtPhotElectProc = "PMTPhotElectProc";
const G4String HpdPhotElectProc = "HPDPhotElectProc";
const G4String PMTEnvelopeMaterialName = "Kovar";
const G4String GasQuWinPhysName = "GasQuWinPhys";
const G4String RadiatorPhysName = "RadiatorPhys";
const G4String PMTFrontRingBoxPhysName = "PMTFrontRingPhys";
const G4String PMTQwLogVolName = "PMTQuartzWLog";
const G4String PMTPhCathLogVolName = "PMTPhCathodeLog";
const G4String PMTNumberName[] = {"_0", "_1", "_2",  "_3",  "_4",  "_5",  "_6",  "_7",
                                  "_8", "_9", "_10", "_11", "_12", "_13", "_14", "_15"};

const G4String HpdQuartzWMaterialName = "HpdQuartzPhys";
const G4String HpdPhCathodeMaterialName = "HpdPhcathodePhys";
const G4String HpdSiDetMaterialName = "HpdAnode";
const G4String HpdEnvelopeMaterialName = "Kovar";

#endif // INCLUDE_RICHTBMISCNAMES_HH
