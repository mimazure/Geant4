#ifndef RichTbPmtSteppingAction_h
#define RichTbPmtSteppingAction_h 1
#include "G4UserSteppingAction.hh"
#include "G4VDiscreteProcess.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleChange.hh"
#include "RichTbMiscNames.hh"

class RichTbPmtSteppingAction : public G4UserSteppingAction {

public:
  RichTbPmtSteppingAction();
  virtual ~RichTbPmtSteppingAction();
  void UserSteppingAction(const G4Step *aStep) override;

private:
  G4double mChTrackMinMomFactorForHisto;
};
#endif
