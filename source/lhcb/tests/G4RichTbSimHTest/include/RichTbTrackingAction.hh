#ifndef RichTbTrackingAction_h
#define RichTbTrackingAction_h 1

#include "G4UserTrackingAction.hh"
#include "G4Track.hh"

class RichTbTrackingAction : public G4UserTrackingAction {

public:
  RichTbTrackingAction();
  virtual ~RichTbTrackingAction();
  void PreUserTrackingAction(const G4Track *aTrack) override;

  void PostUserTrackingAction(const G4Track *aTrack) override;

private:
};
#endif
