#ifndef RichTbSteppingAction_h
#define RichTbSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "G4VDiscreteProcess.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleChange.hh"
#include "RichTbMiscNames.hh"

class RichTbSteppingAction : public G4UserSteppingAction {

public:
  RichTbSteppingAction();
  virtual ~RichTbSteppingAction();
  void UserSteppingAction(const G4Step *aStep) override;
  void RichTbGenericHisto(const G4Step *aStep);
  void RichTbDebugHisto(const G4Step *aStep);
  void RichTbPMTIncidenceStep(const G4Step *aStep);
  void RichTbAgelIncidenceStep(const G4Step *aStep);
  void RichTbRadiatorLensBoundaryIncidenceStep(const G4Step *aStep);

  //    G4double getPMTPhElectronKE() {
  //      return PMTPhElectronKE;
  //  }

private:
  //    G4double PMTPhElectronKE;
  // G4VParticleChange *uParticleChange;

  double RichTbRadiatorXNegExtreme_;
  double RichTbRadiatorXPosExtreme_;
  double RichTbRadiatorYNegExtreme_;
  double RichTbRadiatorYPosExtreme_;
  double RichTbRadiatorZNegExtreme_;
  double RichTbRadiatorZPosExtreme_;
  double RichTbRadiatorDnsZLocation_;
};
#endif
