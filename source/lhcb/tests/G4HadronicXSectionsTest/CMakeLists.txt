add_executable(G4HadronicXSectionsTest
    G4HadronicXSectionsTest.cc
    src/ActionInitialization.cc
    src/DetectorConstruction.cc
    src/DetectorMessenger.cc
    src/EventAction.cc
    src/EventActionMessenger.cc
    src/HistoManager.cc
    src/HistoManagerMessenger.cc
    src/PrimaryGeneratorAction.cc
    src/RunAction.cc
)
target_include_directories(G4HadronicXSectionsTest
    PRIVATE include
)
target_link_libraries(G4HadronicXSectionsTest
    PRIVATE
        G4global
        G4physicslists
        G4run
)
install(TARGETS G4HadronicXSectionsTest)
install(
    PROGRAMS
        scripts/G4HadronicXSectionsTest.py
    TYPE BIN
)
